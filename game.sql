-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Maio-2018 às 02:49
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `game`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `familia`
--

CREATE TABLE `familia` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `quantidade_membros` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `familia`
--

INSERT INTO `familia` (`id`, `nome`, `quantidade_membros`) VALUES
(9, 'coxinha', 7),
(11, 'lucas', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `familia_has_guerra`
--

CREATE TABLE `familia_has_guerra` (
  `familia_id` int(11) NOT NULL,
  `guerra_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `guerra`
--

CREATE TABLE `guerra` (
  `id` int(11) NOT NULL,
  `id_familia_desafiadora` varchar(45) NOT NULL,
  `id_familia_desafiada` varchar(45) NOT NULL,
  `data_inicio` date DEFAULT NULL,
  `data_fim` date DEFAULT NULL,
  `id_familia_vencedora` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `guerra`
--

INSERT INTO `guerra` (`id`, `id_familia_desafiadora`, `id_familia_desafiada`, `data_inicio`, `data_fim`, `id_familia_vencedora`) VALUES
(2, '8', '18', '2018-05-23', '2018-07-01', '3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `familia_has_guerra`
--
ALTER TABLE `familia_has_guerra`
  ADD PRIMARY KEY (`familia_id`,`guerra_id`),
  ADD KEY `fk_familia_has_guerra_guerra1_idx` (`guerra_id`),
  ADD KEY `fk_familia_has_guerra_familia_idx` (`familia_id`);

--
-- Indexes for table `guerra`
--
ALTER TABLE `guerra`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `familia`
--
ALTER TABLE `familia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `guerra`
--
ALTER TABLE `guerra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `familia_has_guerra`
--
ALTER TABLE `familia_has_guerra`
  ADD CONSTRAINT `fk_familia_has_guerra_familia` FOREIGN KEY (`familia_id`) REFERENCES `familia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_familia_has_guerra_guerra1` FOREIGN KEY (`guerra_id`) REFERENCES `guerra` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
