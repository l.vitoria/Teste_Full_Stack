<?php

class familia{

	private $id;
	private $nome;
	private $quantidade_membros;

	public function setId($id) {
		$this->id=$id;
	}
	
	public function getId() {
		return $this->id;
	}

	public function setNome($nome) {
		$this->nome=$nome;
	}
	
	public function getNome() {
		return $this->nome;
	}

	public function setQuantidade_membros($quantidade_membros) {
		$this->quantidade_membros=$quantidade_membros;
	}
	
	public function getQuantidade_membros() {
		return $this->quantidade_membros;
	}

}
?>