<?php

class guerra{

	private $id;
	private $id_familia_desafiadora;
	private $id_familia_desafiada;
	private $data_inicio;
	private $data_fim;
	private $id_familia_vencedora;

	public function setId($id) {
		$this->id=$id;
	}
	
	public function getId() {
		return $this->id;
	}

	public function setId_familia_desafiadora($id_familia_desafiadora) {
		$this->id_familia_desafiadora=$id_familia_desafiadora;
	}
	
	public function getId_familia_desafiadora() {
		return $this->id_familia_desafiadora;
	}

	public function setId_familia_desafiada($id_familia_desafiada) {
		$this->id_familia_desafiada=$id_familia_desafiada;
	}
	
	public function getId_familia_desafiada() {
		return $this->id_familia_desafiada;
	}

	public function setData_inicio($data_inicio) {
		$this->data_inicio=$data_inicio;
	}
	
	public function getData_inicio() {
		return $this->data_inicio;
	}

	public function setData_fim($data_fim) {
		$this->data_fim=$data_fim;
	}
	
	public function getData_fim() {
		return $this->data_fim;
	}

	public function setId_familia_vencedora($id_familia_vencedora) {
		$this->id_familia_vencedora=$id_familia_vencedora;
	}
	
	public function getId_familia_vencedora() {
		return $this->id_familia_vencedora;
	}

}
?>