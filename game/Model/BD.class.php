<?php

class BD {

    var $host = "localhost";
    var $usuario = "root";
    var $senha = "";
    var $db = "game";
    

  
     public function __construct() {
        try {
           $this->pdo = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db, $this->usuario, $this->senha);
           $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

       } catch (PDOException  $e) {
           print $e->getMessage();
       }
   }
    
 }


