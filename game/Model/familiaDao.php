<?php

require_once 'BD.class.php';

class familiaDao extends BD{
private $bd; //conexão com o banco
private $tabela; //nome da tabela

public function __construct() {
  $this->bd = new BD();
  $this->tabela = "familia";
}

public function inserir($familia) {

  $nome=$familia->getNome();
  $quantidade=$familia->getQuantidade_membros();


  $sql = ("INSERT INTO $this->tabela (nome,quantidade_membros ) values ( :nome,:quantidade)");  


  $retorno = $this->bd->pdo->prepare($sql);
  $retorno->bindParam(':nome', $nome);
  $retorno->bindParam(':quantidade', $quantidade);


           //print_r($sql); die;

  return $retorno->execute();

}


public function getAll(){
  $resultado = $this->bd->pdo->query("SELECT * FROM $this->tabela");
  $dados= $resultado->fetchAll(); 

        //print_r($resultado); 
  return $dados;

}


public function excluir($id) {

  $sql = "delete from $this->tabela where id='$id'";
  $retorno= $this->bd->pdo->exec($sql);
        //$retorno = pg_query($sql);
  return $retorno;

}

public function listar($id){
  try {

           // echo $cd_usuario;
    $sql = "SELECT * FROM $this->tabela WHERE id = :cod";


    $res  =$this->bd->pdo->prepare($sql);

    $res->bindValue(':cod', $id);
    $res->execute();

    $linha= $res->fetch();

          //print_r($linha);
    return $linha;


  } catch ( PDOException  $e) {

   print "Erro: Código:" . $e->getCode() . "Mensagem" . $e->getMessage(); }


 }


 public function editar($familia){
     //   var_dump($listar);
  $nome=$familia->getNome();
  $quantidade=$familia->getQuantidade_membros();
  $id=$familia->getId();


  $msq = "UPDATE $this->tabela SET nome=:nome, quantidade_membros=:quantidade where id=:id";

  $retorno = $this->bd->pdo->prepare($msq);
  $retorno->bindParam(':nome', $nome);
  $retorno->bindParam(':quantidade', $quantidade);
  $retorno->bindParam(':id', $id);


  return $retorno->execute();
}


}

?>