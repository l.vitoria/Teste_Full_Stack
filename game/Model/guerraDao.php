<?php

require_once 'BD.class.php';

class guerraDao extends BD{
private $bd; //conexão com o banco
private $tabela; //nome da tabela

public function __construct() {
	$this->bd = new BD();
	$this->tabela = "guerra";
}

public function inserir($guerra) {

	$desafiadora=$guerra->getId_familia_desafiadora();
	$desafiada=$guerra->getId_familia_desafiada();
	$Data_inicio=$guerra->getData_inicio();
	$Data_fim=$guerra->getData_fim();
	$vencedora=$guerra->getId_familia_vencedora();

	$sql = ("INSERT INTO $this->tabela (id_familia_desafiadora,id_familia_desafiada,data_inicio,data_fim,id_familia_vencedora ) values ( :desafiadora,:desafiada,:Data_inicio,:Data_fim,:vencedora)");  


	$retorno = $this->bd->pdo->prepare($sql);
	$retorno->bindParam(':desafiadora', $desafiadora);
	$retorno->bindParam(':desafiada', $desafiada);
	$retorno->bindParam(':Data_inicio', $Data_inicio);
	$retorno->bindParam(':Data_fim', $Data_fim);
	$retorno->bindParam(':vencedora', $vencedora);



	return $retorno->execute();

}


public function getAll(){
	//echo "<script>alert('entro aqui!');</script>";
	$resultado = $this->bd->pdo->query("SELECT * FROM $this->tabela");
	$dados= $resultado->fetchAll(); 

        //print_r($resultado); 
	return $dados;

}


public function excluir($id) {

	$sql = "delete from $this->tabela where id='$id'";
	$retorno= $this->bd->pdo->exec($sql);
        //$retorno = pg_query($sql);
	return $retorno;

}

public function listar($id){
	try {

           // echo $cd_usuario;
		$sql = "SELECT * FROM $this->tabela WHERE id = :cod";


		$res  =$this->bd->pdo->prepare($sql);

		$res->bindValue(':cod', $id);
		$res->execute();

		$linha= $res->fetch();

          //print_r($linha);
		return $linha;


	} catch ( PDOException  $e) {

		print "Erro: Código:" . $e->getCode() . "Mensagem" . $e->getMessage(); }


	}


	public function editar($guerra){

		$id=$guerra->getId();
		$desafiadora=$guerra->getId_familia_desafiadora();
		$desafiada=$guerra->getId_familia_desafiada();
		$Data_inicio=$guerra->getData_inicio();
		$Data_fim=$guerra->getData_fim();
		$vencedora=$guerra->getId_familia_vencedora();
		
		$sql = ("UPDATE $this->tabela  SET id_familia_desafiadora=:desafiadora, id_familia_desafiada=:desafiada, data_inicio=:Data_inicio, data_fim=:Data_fim, id_familia_vencedora=:vencedora where id=:id");  


		$retorno = $this->bd->pdo->prepare($sql);
		$retorno->bindParam(':id', $id);
		$retorno->bindParam(':desafiadora', $desafiadora);
		$retorno->bindParam(':desafiada', $desafiada);
		$retorno->bindParam(':Data_inicio', $Data_inicio);
		$retorno->bindParam(':Data_fim', $Data_fim);
		$retorno->bindParam(':vencedora', $vencedora);



		return $retorno->execute();
	}


	public function listarUm($id) {
		/*echo "<script>alert('entro aqui listar um!');</script>";
		print_r($id);
		die;*/
		$sql = "SELECT * FROM $this->tabela where id=:id";
		$retorno  =$this->bd->pdo->prepare($sql);
		$retorno->bindParam(':id', $id);
		$retorno->execute();
		return $retorno->fetchAll(PDO::FETCH_ASSOC);


	}  


}

?>