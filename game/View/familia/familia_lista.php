<!DOCTYPE html>
<html>
<head>
	<title>lista da familia</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">    
	<meta name="description" content="Lista de produtos da tabela produtos">
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<style type="text/css">
	.but{

		margin-bottom: 3px !important;
	}
	.pos{
		margin-left: 67%;
	}
</style>
</head>
<body>
	<div class="container">

		<div class="row">
			<h1>Familia</h1>


			<a  href="../controler/controle_gue.php" class="pos btn btn-success text-white">Combate</a>

			<input  style="width: 100%;" class=" w3-input w3-border w3-padding" type="text" placeholder="Procure pelo nome de sua familia"  id="mybut" onkeyup="myFunction()">

			<table class="table table-bordered" id="myTably">

				<thead>
					<tr class="text-center">
						<th class=" bg-primary text-white">familia</th>
						<th class=" bg-primary text-white">membros</th>
						<th class=" bg-primary text-white">guerras</th>
						<th class=" bg-primary text-white">vitoria</th>
						<th class=" bg-primary text-white">derrotas</th>
						<th class=" bg-primary text-white">ações</th>
					</tr>
				</thead>

				<?php
				$contador = 0;
				
				foreach ($listar as $listar)
				{ 
					?>       
					<tr>
						<td class="text-center"> <?php echo $listar['nome'] ?></td> 
						<td class="text-center "><?php echo $listar['quantidade_membros']?></td>
						<td class="text-center">
							<td class="text-center">
								<td class="text-center">
									<td>
										<a href="../controler/controle_fam.php?tag=editar&amp;id=<?php echo $listar['id']?>" class="btn btn-primary col-sm-10">Editar</a>
										<br>
										<a href="../controler/controle_fam.php?tag=excluir&amp;id=<?php echo $listar['id']?>"  class="btn btn-danger col-sm-10">Excluir</a>
									</td>
								</td> 
							</tr>

							<?php
							$contador++;
						}
						?>

					</table>

					<div class="row">
						<div class="col-md-12">
							Todal de casas: <?php echo $contador ?>
						</div>
					</div>
					<!--modal escondido-->
					<button onclick="document.getElementById('id01').style.display='block'" class=" but pos w3-button w3-green w3-large">adicionar familia</button>
				</div>

				<!--modal aparecendo-->
				<div id="id01" class="w3-modal">
					<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

						<div class="w3-center w3-animate-zoom"><br>
							<span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>




							<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PEA8OEBAPEBAQEA8PEBAQDxAQDxANFREWFhYVFRUYHSgiGBomGxMVITEiJSk3LjoxFyAzODMsNygwLisBCgoKDg0NGg0PDy0ZHx4rKys3KysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIDBAUGBwj/xAA/EAACAgECBAIHAgoLAQAAAAAAAQIDEQQSBSExQQYTFCJRYXGBkSPRFSQlMkJicqGy8DM0c4KDoqSxwcLSB//EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFREBAQAAAAAAAAAAAAAAAAAAAAH/2gAMAwEAAhEDEQA/APDQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASivZy6e75siB7zwvwvo5eHVW7oQVjjfO17W3bsztz2eenwQHgYKp9SkAAAAAAAAAAAAAAAAAAAAAAAAAAAAJGAIBOBgCAABKZ3PD/ABTKHB7tD5ae6NsFa5c4QnbGTW3Huxn3nCm1pf4tJe5v/OgNZJ8ykAAAAAAAAAAAAAAAAAAAAAAAAAAAAJyMkACcjJAAqwQyqCM+nhGotSdVF1mejhVOSfzSA1ptql+Lv+zl/GmZEfCOvaT9FuX7SjF/RtHTaLwDqpUOO6tXei2XeQ8+Y3u5Vp9N7x0JR52DoZeCuJJZ9Eua92xv6JmFq/D+spTlZpdRCK6ylTNRXxbXIo1YKmiccgKAAAAAAAAAAAAAAAACqCy8FJ0XgjTRs1TW1SsjptbZp4NJqeshppyqWH1e5Jpe2KA1D4ddhtVWNLLbUJckll5+RjuJveAvVajUShXqXXN06myUpzt2zhXTOycXtT6xi+T5dDR9P3ARsfXHLnz7csfeUs7vU8Ng+H26NOl6jRwhr9sc+dmaxqoy9yjKmX+EzhpdQDg1jKa5J9Oz6Mny+mU+fNe9e76P6Hfce4N6XGiNSjG/Qx0mi1GMc9NKiEoXyX6svMjL+4anxnslVw2+pKNctLbp4JLn9hq7oqTfduMoNv2tgc1Rp5TztjKWFlqMW3j28iLqJQ5SjKMuXKSaf0Z1HgCXr6/MpRX4N1eZQWZLDrw0srL+ZoOK2KU8q625bYpTti4z6ZcWt0ujeOrAxKVzM/VcU1GyNPnWuuCxGDnLZFdcKPQu8B4art85ScYQlTB7VmbndPZFLsujefd0eToI+EoThZdutsgoQlXCCrjY27/KmpbuXLrnvkDj4WNvm2/i2+R9A8L4jZHhll2Y71p51qeyO9RV9UIrOPZYz5+nyfI9no1sY8InHq3GS/1ml+8ix47qdRNW2bZTXrz6Skv0mZf4S1tcfK83UQjOLzBucVKE0k8p/nZS95r9RN7p/tT/AN2db4wVDnpHbPUKxcN4c/s4VSj/AFeOMNzT9pUcnbRKL2yjKMu6cWmvinzKq9NOa9WE5Y5Nxi5JPtzSOg/+kWflTW9f6WPN9ceXAv8Ahdt8N4mlcqPtNDiyTsUYvze7gm18kByNtbi8NNdVzWOa6lWnoc3iKbfZJNt/BI3Xjre9ddKcdrcaUpeq1co0wh56ceUlZt35XL1mZWgr2cIv1FT+1lq403SXKVen8pyis9lKXL9wGgv0M62o2Vzg303RlHP1SLNdTk9sU23hJJZbefZ3fwPRONah3Q47C9uUKlpLtO5PLhqZOEdsW+frQcsr9U5XwXJ/hHRuLxJaiDT/AFuz+oGmrolNqMYuTfRR5tv4IpcMPGHn2Psd9wauvW6nTcQ00VXqKrapa7TQ5YTkl6TTFda236yXOLa7PJw2pf2k+f6U/wCJgT6Da1uVc3HG7KjJrau+cFhQ/nsdjxK2K4dwnN99cvRtbtjCvdCf45akpPesdl0Zyulm4ThNRUnCUZbWm09sk8Nd1yApnpZxWZQkl2bi0m/iyxI6nxI43VemUXTlRbelZprZN2abVODe3P5s4NKW2S7LDSwctLqBAAAF7S6idU4WVycZwkpQlF4lGa5pp+3JZAG/n4nucpWbNPG2cLK53RpgrnCytwn6yXVxlJZx3NVp7XXOM0otxkmk+ayn3T+RigDbafjd9eolq4ySunv3PanGSmsSW3pjHYwJTWc8ub6JcvkvmWABuLOPah2X2qzbLUQdV2xKCnW0sppfBfQs38TslRVppOLrolY6vVW6Dsac1u9jaXI1oA2fCuK26V2Sq25tqlTPfGM1KqXWOH25L6GNrtU7WpNQjhKKVcFCKis9l8TFJQGdwriVunlKVbXPapRkt0JYknHK9qaTR0Oj4hPUabXuxrlXpUorlGK9Iy8R7c8s5AuUzaTWcKXJ+/4gVzW58vgvez0/TT/JsorLTclntn0nTP8A6nmUU+3Q7HQ69PSShvS2yTw+vO2r/wAsDlbaPWefbJ/HmZHE+I2aiUJ2bG6666Y4gop1wSUYvHXCWC5ZdvjlxWefPvg1trAv8Y4jZq7Z6i5qVs2t0oxUdzSSzhe4afidldN2mi4+Ve4eYnFNtweYvd2wYMkUgb+HFKr9LHS6lSUqM+i6iCU5Qh1dNibTdeW2mnlPszX8O4pbpvM8trFq2W1zipV2R7KUXyfM1wA3HFuP6jVOXmygtzjOSrhGuEpxikpSSxlpLGTE4brrNPbC6pqNlclKEmk8SXR4ZhADO0PEbaLY30zdVkW3GUPVxlYa+HuMffltvnnq/eWQBuPw/d5VVDVLhRGddWaoSlCE5OUsSx3cm/mYOk1Eqpxsg0pRacc4eGviYoA2Gv4nZctslCMN2/ZXCNcHZjG5pdXgwGQAAAAAAAAAAAAAAASiABJXUlnn0KEXIAbKuDcdq6f8F62qVaSx+fyyu+HFljR6lR6l2V0ZpbpNpZ2ruucfuAwIXOK9/T5FqUhbHDLaYCTKckspAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABcTLZKAubiuE/5+n3FlDIFybyW2SUsCclIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJRAAkZBAE5IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlDJAAnJAAAAAAAAAAAAAAAAAAAAAAAABOCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLu4hOalFqvEuuIRi/3dDEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/Z" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
						</div>

						<form class="w3-container" action="../../../game/controler/controle_fam.php" method="post">
							<div class="w3-section">


								<label><b>Nome da familia</b></label>
								<input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Nome da familia" name="nome" required>



								<label><b>quantidade de membros</b></label>
								<input class="w3-input w3-border" type="text" placeholder="quantidade de membros" name="quantidade" required>



								<button class="w3-button w3-block w3-green w3-section w3-padding" type="submit" name="acao" value="Cadastrar">cadastrar</button>

							</div>
						</form>

						<div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
							<button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-button w3-red">Cancel</button>

						</div>

					</div><!-- /.container -->
				</div>


    <!-- Bootstrap core JavaScript
    	================================================== -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>   
    </body>
    <script type="text/javascript">
    	function myFunction() {
    		var input, filter, table, tr, td, i;
    		input = document.getElementById("mybut");
    		filter = input.value.toUpperCase();
    		table = document.getElementById("myTably");
    		tr = table.getElementsByTagName("tr");
    		for (i = 0; i < tr.length; i++) {
    			td = tr[i].getElementsByTagName("td")[0];
    			if (td) {
    				if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
    					tr[i].style.display = "";
    				} else {
    					tr[i].style.display = "none";
    				}
    			}
    		}
    	}
    </script>

</body>
</html>