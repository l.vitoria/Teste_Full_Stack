<!DOCTYPE html>
<html>
<head>
	<title>Lista de combates</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">    
	<meta name="description" content="Lista de produtos da tabela produtos">
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<style type="text/css">
	.pos{
		margin-left: 67%;
	}
	.pose{
		margin-left: 39%;
	}
</style>
</head>
<body>
	<div class="container">

		<div class="row">
			<h5>Bem vindo aos combates:</h5>
			<table class="table table-bordered">

				<thead>
					<tr class="text-center">
						<th class=" bg-primary text-white">desafiadora</th>
						<th class=" bg-primary text-white">desafiada</th>
						<th class=" bg-primary text-white">inicio</th>
						<th class=" bg-primary text-white">fim</th>
						<th class=" bg-primary text-white">vencedora</th>
						<th class=" bg-primary text-white">ações</th>
					</tr>
				</thead>

				<?php


				foreach ($listar as $listar)
				{ 
					?>       
					<tr>
						<td class="text-center"> <?php echo $listar['id_familia_desafiadora'] ?></td> 
						<td class="text-center "><?php echo $listar['id_familia_desafiada']?></td>
						<td class="text-center"><?php echo $listar['data_inicio']?></td>
						<td class="text-center"><?php echo $listar['data_fim']?></td>
						<td class="text-center"><?php echo $listar['id_familia_vencedora']?></td>
						<td>
							<a href="../controler/controle_gue.php?tag=editar&amp;id=<?php echo $listar['id']?>" class="btn btn-primary col-sm-10">Editar</a>
							<br>
							<a href="../controler/controle_gue.php?tag=excluir&amp;id=<?php echo $listar['id']?>"  class="btn btn-danger col-sm-10">Excluir</a>
						</td>
					</td> 
				</tr>

				<?php 
			}
			?>


		</table>
	</div>
</div>
<button type="button" class=" pos btn btn-primary" data-toggle="modal" data-target="#exampleModal">
	cadastrar nova guerra
</button>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">cadastro</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="../../game/controler/controle_gue.php" method="post">

					<div class="form-group">
						<label for="exampleInputEmail1">Nome da familia desafiadora</label>
						<input type="text" class="form-control" name="desafiadora" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="familia desafiadora">
					</div>

					<div class="form-group">
						<label for="exampleInputPassword1">Nome da familia desafiada</label>
						<input type="text" name="desafiada" class="form-control" id="exampleInputPassword1" placeholder="familia desafiada">
					</div>

					<div class="form-group">
						<label for="exampleInputPassword1">data de inicio da guerra</label>
						<input type="date" name="inicio" class="form-control" id="exampleInputPassword1" >
					</div>

					<div class="form-group">
						<label for="exampleInputPassword1">data do final da guerra</label>
						<input type="date" name="fim" class="form-control" id="exampleInputPassword1" >
					</div>

					<div class="form-group">
						<label for="exampleInputPassword1">Nome da familia vencedora</label>
						<input type="text" name="vencedora" class="form-control" id="exampleInputPassword1" placeholder="familia vencedora">
					</div>



					<button type="submit" name="acao" value="Cadastrar" class="pose btn btn-primary">Submit</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>
				<!--<button type="button" class="btn btn-primary">cadastrar</button>-->
			</div>
		</div>
	</div>
</div>

</body>
</html>